// manager.h
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "inventory.h"
#include "sales.h"


std::map<std::string, struct product_desc> load_inventory(){
    std::string products_file = "products.csv";
    std::map<std::string, struct product_desc> products = load_products(products_file);
    return products;
}

int backup_inventory(std::map<std::string, struct product_desc> products){ 
    std::string products_file = "products.csv";
    int v = backup_products(products, products_file);

    return v;
}

int sell(std::string* id, int quantity, std::string pos){
    int v;
    std::string sales_file = "sales.csv";

    std::map<std::string, struct product_desc> products = load_inventory();
    if (products[pos].amount < quantity){
        return -1;
    }

    std::string type = products[pos].type;
    int price = products[pos].price;
    int amount = products[pos].amount - quantity;

    if (amount == 0){
        delete_product(products, id, pos);
    } else {
        struct product_desc buffer = { type, price, amount };
        products[pos] = buffer; 
    }


    v = register_sale(pos, type, price, quantity, sales_file);
    if (v == 1) return v;

    v = backup_inventory(products);
    return v;
}

int restock(std::string* id, int quantity){
    std::string products_file = "products.csv";
    std::map<std::string, struct product_desc> products = load_inventory();
    // if (products == NULL){
    //     return 1;
    // }

    std::string type = products[*id].type;
    int price = products[*id].price;
    int amount = products[*id].amount + quantity;
    struct product_desc buffer = { type, price, amount };
    products[*id] = buffer; 

    int v = backup_inventory(products);

    return v;
}
int add(std::string* id, std::string type, int amount, int price){
    std::map<std::string, struct product_desc> products = load_inventory();
    // if (products == NULL){
    //     return 1;
    // }

    products = create_product(type, price, amount, products, id);

    int v = backup_inventory(products);

    return v;
}
int update(std::string* id, int quantity){
    std::map<std::string, struct product_desc> products = load_inventory();
    // if (products == NULL){
    //     return 1;
    // }

    std::string type = products[*id].type;
    int price = products[*id].price;
    int amount = quantity;
    struct product_desc buffer = { type, price, amount };
    products[*id] = buffer; 

    int v = backup_inventory(products);

    return v;
}

std::string view_prod_stock_lvls(void){
    std::map<std::string, struct product_desc> products = load_inventory();

    std::string buffer = "";

    for (auto it = products.cbegin(); it != products.cend(); ++it) {
        buffer = buffer + it->first + "," + it->second.type + "," + std::to_string(it->second.price) + "," + std::to_string(it->second.amount) + "\n";  
    }
    
    return buffer;
}

std::string sales_rep(void){
    std::string sales_file = "sales.csv";
    std::string buffer = read_sales(sales_file);

    return buffer;
}

std::string prod_get_id(){
    std::string id;
    std::string products_file = "products.csv";

    id = get_id(products_file);

    return id;
}