// sales.h
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

std::string read_sales(std::string sales_file){
    std::ifstream csv(sales_file);
    std::string buffer, it = "";

    if (!csv) {
    } else {
        while (std::getline(csv, it)) {
            buffer = buffer + it + "\n";
        }
    }
    return buffer;
}

int register_sale(std::string pos, std::string type, int price, int amount, std::string sales_file){
    std::fstream csv;
    int total = price * amount;
    
    csv.open(sales_file, std::ios_base::out | std::ios_base::app);
    if (!csv.is_open()){
        return 1;
    } else {
        csv << pos << "," << type << "," << price << "," << amount  << "," << total << "\n";  
    }
    csv.close();
    
    return 0;
}