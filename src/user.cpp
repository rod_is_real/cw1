// user.cpp - frontend user interface
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

#include "manager.h"

#define clear() printf("\033[2J\033[1;1H");

int display_menu(void){
    std::cout << "Stock Manager Software\n----\n" <<
        "1. Sell product\n" <<
        "2. Update product\n" <<
        "3. Restock product\n" <<
        "4. Add new product\n" <<
        "5. View product stock levels\n" <<
        "6. Display sales report\n" <<
        "0. Quit\n";

    return 0;
}

bool check_int_input(std::string check_int){
    int i;
    bool flag = true;

    for (i = 0; i < check_int.length(); i++){
        if (isdigit(check_int[i]) == false){
            flag = false;
            break;
        }
    }
    flag = !flag;
    return flag;
}

int get_user_input(void){
    int choice;
    bool flag;
    std::string check_int;

    std::cout << ">>> ";
    std::cin >> check_int;

    flag = check_int_input(check_int);
    if (flag == true){
        choice = 420;
    } else {
        choice = stoi(check_int);
    }

    return choice;
}

int user_input(void){
    clear();
    display_menu();

    int choice = get_user_input();
    return choice;
}

int user_quit(void){
    std::cout << "[*] Gracefully quitting...";
    exit(0);
}

int user_sell(std::string* id){
    int quantity;
    std::string pos;
    bool flag = true;
    std::string check_int;
   
    while (flag){
        std::cout << "Enter id [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);

    }
    pos = check_int;

    flag = true;
    while (flag){
        std::cout << "Enter quantity [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);

    }
    quantity = stoi(check_int);


    sell(id, quantity, pos);
    return 0;
}

int user_update(std::string* id){
    int quantity;
    bool flag = true;
    std::string check_int;

    while (flag){
        std::cout << "Enter id [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);

    }
    *id = check_int;

    flag = true;
    while (flag){
        std::cout << "Enter quantity [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);

    }
    quantity = stoi(check_int);


    update(id, quantity);
    return 0;
}

int user_restock(std::string* id){
    int quantity;
    bool flag = true;
    std::string check_int;
    
    while (flag){
        std::cout << "Enter id [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);

    }
    *id = check_int;

    flag = true;
    while (flag){
        std::cout << "Enter quantity [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);

    }
    quantity = stoi(check_int);

    restock(id, quantity);
    return 0;
}


int user_add(std::string* id){
    std::string type;
    int price;
    int amount;
    
    bool flag = true;
    std::string check_int;
   

    while (flag){
        std::cout << "Types = [Toy | Accessorie | Book | PetFood]" << std::endl;
        std::cout << "Enter type: ";
        std::cin >> type;

        if (type == "Toy" || type == "Accessorie" || type == "Book" || type == "PetFood"){
            flag = false;
        }
    }
 
    flag = true;
    while (flag){
        std::cout << "Enter price [integer]: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);
    }
    price = stoi(check_int);

    flag = true;
    while (flag){
        std::cout << "Enter amount: ";
        std::cin >> check_int;
        flag = check_int_input(check_int);
    }
    amount = stoi(check_int);

    add(id, type, amount, price);
    return 0;
}

int user_view_prod_stock_lvls(void){
    std::string stock;
    stock = view_prod_stock_lvls();

    std::cout << "Stock Manager" << "\n" << "--------" << "\n" << stock << std::endl;
    sleep(5);

    return 0;
}

int user_sales_rep(void){
    std::string sales;
    sales = sales_rep();

    std::cout << "Sales Report" << "\n" << "--------" << "\n" << sales << std::endl;
    sleep(5);

    return 0;
}

int user_wrong_input(void){
    // (testing) this may be needed to correct input if ran as main 
    //std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "[x] Wrong input, try again...\n";
    sleep(1);
    return 0;
}

int handle_user_input(int choice, std::string* id ){
    choice == 0 ? user_quit() :
    choice == 1 ? user_sell(id) :
    choice == 2 ? user_update(id) :
    choice == 3 ? user_restock(id) :
    choice == 4 ? user_add(id) :
    choice == 5 ? user_view_prod_stock_lvls() :
    choice == 6 ? user_sales_rep() :
    user_wrong_input();
    return 0;
}

int main(void){
    std::string* id;

    *id = prod_get_id();

    while (1){
        handle_user_input(user_input(), id);
    }
}
