// inventory.h - backend database management
// database is a csv file
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

struct product_desc {
    std::string type;
    int price;
    int amount;
};

int count_lines(std::string filename){
    std::ifstream csv(filename);
    std::string unused;
    int c = 0;

   if (!csv){
       return -1;
   } else{
        while (std::getline(csv, unused)) ++c;
        c++;
   }

    return c;
}

std::string get_id(std::string filename){
    std::string id;
    std::fstream csv;

	csv.open(filename, std::ios::in);
    id = std::to_string(count_lines(filename));
    if (id == "-1"){
        id = "0";
    } else {
        csv.close();
    }

    return id;
}

std::map<std::string, struct product_desc> create_product(std::string type, int price, int amount, std::map<std::string, struct product_desc> products, std::string* id){
    struct product_desc buffer = { type, price, amount };
    products[*id] = buffer;

    *id = std::to_string(stoi(*id) + 1);
    return products;
}

struct product_desc parse(std::string it){
    const char comma = ',';
    std::istringstream istr_it(it);
    std::string token;
    std::string type;
    int price;
    int amount;

    int i  = 0;
    while (std::getline(istr_it, token, comma)){
        if (i == 1){
            type = token;
        }
        if (i == 2){
            std::istringstream ( token ) >> price;
        }
        if (i == 3){
            std::istringstream ( token ) >> amount;
        }
        ++i;
    }

    struct product_desc buffer = { type, price, amount };
    return buffer;
}

std::map<std::string, struct product_desc> delete_product(std::map<std::string, struct product_desc> products, std::string* id, std::string pos){
    std::string map_key, it, type;
    int price, amount;

    int map_size = products.size();
    int i = stoi(pos);
    
    while (i < map_size - 1){
        it = std::to_string(i + 1);
        type = products[it].type;
        price = products[it].price;
        amount = products[it].amount;
        struct product_desc buffer = { type, price, amount };

        it = std::to_string(i);
        products.erase(it);
        products[it] = buffer;

        ++i;
    }

    products.erase(prev(products.end()));
    *id = std::to_string(stoi(*id) - 1);

    return products;
}

std::map<std::string, struct product_desc> load_products(std::string products_file){
    std::map<std::string, struct product_desc> products;

    std::string si;
    int i = 0;
    
    std::ifstream csv(products_file);
    std::string it;
    if (!csv) {
    } else {
        while (std::getline(csv, it)) {
            ++i;
            si = std::to_string(i);
            
            struct product_desc buffer = parse(it);
            products[si] = buffer;
        }
    }
    csv.close();
    return products;
}

int backup_products(std::map<std::string, struct product_desc> products, std::string products_file){
    std::fstream csv;
    
    csv.open(products_file, std::ios_base::out);
    if (!csv.is_open()){
        return 1;
    } else {
        for (auto it = products.cbegin(); it != products.cend(); ++it) {
            csv << it->first << "," << it->second.type << "," << it->second.price << "," << it->second.amount << "\n";  
        }
    }
    csv.close();
    return 0;
}
